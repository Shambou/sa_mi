document.addEventListener("DOMContentLoaded", () => {
    let loginForm = document.getElementById("loginForm")
    let loginReq = {
        "email": "",
        "password": "",
    }
    if (loginForm) {
        loginForm.addEventListener("submit", (e) => {
            e.preventDefault();
            loginReq = parseFieldsIntoObject(loginReq, loginForm.elements)

            fetch('/api/login', {
                headers: { "Content-Type": "application/json; charset=utf-8" },
                method: 'POST',
                body: JSON.stringify(loginReq)
            }).then(async res => {
                let data = await res.json();
                if (data.status !== 200) {
                    let validationAlert = document.getElementById('validationAlert');
                    validationAlert.innerHTML = data.message;
                    const errors = data.data ? data.data.errors : null;
                    if (errors) {
                        renderErrorList(errors, validationAlert);
                    }
                    validationAlert.classList.remove('hidden');
                } else {
                    localStorage.setItem("access_token", data.data.access_token);
                    localStorage.setItem("user_id", data.data.user_id);
                    window.location.replace('/profile-edit');
                }
            }).catch(err => {
                console.log(err);
            })

        });
    }

    let signupForm = document.getElementById("signupForm")
    let signupReq = {
        "email": "",
        "password": "",
    }
    if (signupForm) {
        signupForm.addEventListener("submit", (e) => {
            e.preventDefault();
            signupReq = parseFieldsIntoObject(signupReq, signupForm.elements)

            fetch('/api/signup', {
                headers: { "Content-Type": "application/json; charset=utf-8" },
                method: 'POST',
                body: JSON.stringify(signupReq)
            }).then(async res => {
                let data = await res.json();
                if (data.status !== 200) {
                    let validationAlert = document.getElementById('validationAlert');
                    validationAlert.innerHTML = data.message;
                    const errors = data.data ? data.data.errors : null;
                    if (errors) {
                        renderErrorList(errors, validationAlert);
                    }
                    validationAlert.classList.remove('hidden');
                } else {
                    window.location.replace('/login');
                }
            }).catch(err => {
                console.log(err);
            })

        });
    }

    let profileForm = document.getElementById("profileEditForm")
    let profileReq = {
        "full_name": "",
        "email": "",
        "phone": "",
    }
    if (profileForm) {
        profileForm.addEventListener("submit", (e) => {
            e.preventDefault();
            profileReq = parseFieldsIntoObject(profileReq, profileForm.elements);
            const userId = localStorage.getItem("user_id");
            const accessToken = localStorage.getItem("access_token");
            fetch('/api/profile/' + userId +'/edit', {
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + accessToken
                },
                method: 'PATCH',
                body: JSON.stringify(profileReq)
            }).then(async res => {
                let data = await res.json();
                if (data.status !== 200) {
                    let validationAlert = document.getElementById('validationAlert');
                    validationAlert.innerHTML = data.message;
                    const errors = data.data ? data.data.errors : null;
                    if (errors) {
                        renderErrorList(errors, validationAlert);
                    }
                    validationAlert.classList.remove('hidden');
                } else {
                    window.location.replace('/profile');
                }
            }).catch(err => {
                console.log(err);
            })
        });
    }

    let logoutBtn = document.getElementById("logoutBtn")
    if (logoutBtn) {
        logoutBtn.addEventListener("click", (e) => {
            e.preventDefault();
            localStorage.clear();
            window.location.replace('/logout');
        });
    }

    function parseFieldsIntoObject(obj, inputs) {
        for (let input of inputs) {
            if (Object.keys(obj).includes(input.name)) {
                obj[input.name] = input.value
            }
        }

        return obj;
    }

    function renderErrorList(errors, parentDiv) {
        let ul = document.createElement('ul');
        ul.setAttribute('id','error-list');

        parentDiv.appendChild(ul);

        for (let index in errors) {
            let li = document.createElement('li');
            li.setAttribute('class','item');

            ul.appendChild(li);

            li.innerHTML= li.innerHTML + index + ': ' + errors[index].join("\n");
        }
    }
});
