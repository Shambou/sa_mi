package render

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path/filepath"
	"time"

	"bitbucket.org/Shambou/coding-challenge/internal/config"
	"bitbucket.org/Shambou/coding-challenge/internal/helpers"
	"bitbucket.org/Shambou/coding-challenge/internal/models"
)

var app *config.AppConfig
var pathToTemplates = "./web/views"
var functions = template.FuncMap{
	"humanDate": HumanDateTime,
}

// NewRenderer - sets the config for the template package
func NewRenderer(a *config.AppConfig) {
	app = a
}

// AddDefaultData - adds data for all templates
func AddDefaultData(td *models.TemplateData, r *http.Request) *models.TemplateData {
	userId := helpers.GetSessionUser(r)
	if userId != 0 {
		td.IsAuthenticated = 1
	}

	td.Flash = helpers.GetFlashMessage(r)

	return td
}

// HumanDateTime - returns time in YYYY-MM-DD format
func HumanDateTime(t time.Time) string {
	return t.Format(time.RFC822)
}

// Template - renders templates using html/template
func Template(w http.ResponseWriter, r *http.Request, tmpl string, td *models.TemplateData) error {
	var templates map[string]*template.Template

	templates, _ = generateTemplates()
	selectedTemplate, ok := templates[tmpl]
	if !ok {
		return errors.New("can't get view template")
	}

	buf := new(bytes.Buffer)

	td = AddDefaultData(td, r)

	err := selectedTemplate.Execute(buf, td)
	if err != nil {
		log.Fatal(err)
	}
	_, err = buf.WriteTo(w)
	if err != nil {
		fmt.Println("Error writing template to browser", err)
		return err
	}

	return nil
}

// generateTemplates - inserts all templates from pathToTemplates to templates map
func generateTemplates() (map[string]*template.Template, error) {
	templates := map[string]*template.Template{}

	pages, err := filepath.Glob(fmt.Sprintf("%s/*.page.tmpl", pathToTemplates))
	log.Println("pages", pages)
	if err != nil {
		return templates, err
	}

	for _, page := range pages {
		name := filepath.Base(page)
		ts, err := template.New(name).Funcs(functions).ParseFiles(page)
		if err != nil {
			return templates, err
		}

		// search for layout files and connect them
		matches, err := filepath.Glob(fmt.Sprintf("%s/layouts/*.layout.tmpl", pathToTemplates))
		if err != nil {
			return templates, err
		}

		if len(matches) > 0 {
			ts, err = ts.ParseGlob(fmt.Sprintf("%s/layouts/*.layout.tmpl", pathToTemplates))
			if err != nil {
				return templates, err
			}
		}

		templates[name] = ts
	}

	return templates, nil
}
