# Coding challenge

Public URL: https://sa-mi-challenge.herokuapp.com/

API documentation URL: https://app.swaggerhub.com/apis/Shambou/coding-challenge/1.0.0

## Running locally
- Copy users table schema provided 
- Copy `env.example` to `.env` and populate env file 
- Running the app locally: `go run ./cmd/server`

### Users table schema:
```mysql
CREATE TABLE `users` (
    `id` bigint unsigned NOT NULL AUTO_INCREMENT,
    `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
    `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
    `external_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
    `created_at` timestamp NOT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
```

