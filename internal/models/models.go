package models

import (
	"time"
)

type User struct {
	ID         int
	Email      string
	FullName   string
	Phone      string
	Password   string
	ExternalId string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
