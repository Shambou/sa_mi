package helpers

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/Shambou/coding-challenge/internal/config"
)

var app *config.AppConfig

// NewHelpers - sets up app config for helpers
func NewHelpers(a *config.AppConfig) {
	app = a
}

// IsAuthenticated - check if we have user_id stored in the session
func IsAuthenticated(r *http.Request) bool {
	session, _ := app.Session.Get(r, "app-session")
	if len(session.Values) == 0 || session.Values["user_id"] == nil {
		return false
	}
	return true
}

// GetSessionUser - gets the user from the session
func GetSessionUser(r *http.Request) int {
	session, _ := app.Session.Get(r, "app-session")
	if len(session.Values) == 0 || session.Values["user_id"] == nil {
		return 0
	}
	return session.Values["user_id"].(int)
}

// GetSessionAccessToken - gets the access_token from the session
func GetSessionAccessToken(r *http.Request) string {
	session, _ := app.Session.Get(r, "app-session")
	if len(session.Values) == 0 || session.Values["access_token"] == nil {
		return ""
	}
	return session.Values["access_token"].(string)
}

// StoreSessionUser - stores user id and access token to the session
func StoreSessionUser(r *http.Request, w http.ResponseWriter, userId int, accessToken string) {
	session, _ := app.Session.Get(r, "app-session")
	session.Options.Path = "/"
	session.Options.Secure = os.Getenv("ENVIRONMENT") == "production"
	session.Options.MaxAge = 3600 * 2 // 2 hours
	session.Options.HttpOnly = true

	if len(session.Values) == 0 {
		session.Values = make(map[interface{}]interface{})
	}

	session.Values["user_id"] = userId
	session.Values["access_token"] = accessToken
	if err := session.Save(r, w); err != nil {
		log.Println("Error saving to session: ", err)
	}
}

// RemoveSessionUser - Removes user session data
func RemoveSessionUser(r *http.Request, w http.ResponseWriter) {
	session, _ := app.Session.Get(r, "app-session")
	session.Options.Path = "/"
	session.Options.Secure = os.Getenv("ENVIRONMENT") == "production"
	session.Options.HttpOnly = true
	session.Options.MaxAge = -1

	session.Values["user_id"] = nil
	session.Values["access_token"] = nil
	if err := session.Save(r, w); err != nil {
		fmt.Println("Could not clear the session", err)
	}
}

// SetFlashMessage - sets the flash message in session
func SetFlashMessage(r *http.Request, w http.ResponseWriter, message string) {
	session, _ := app.Session.Get(r, "flash")
	session.Options.Path = "/"
	session.Options.Secure = os.Getenv("ENVIRONMENT") == "production"
	session.Options.HttpOnly = true
	session.Options.MaxAge = 1

	if len(session.Values) == 0 {
		session.Values = make(map[interface{}]interface{})
	}
	session.Values["flash_message"] = message

	_ = session.Save(r, w)
}

// GetFlashMessage - reads the flash message from session
func GetFlashMessage(r *http.Request) string {
	session, _ := app.Session.Get(r, "flash")
	if len(session.Values) == 0 || session.Values["flash_message"] == nil {
		return ""
	}
	return session.Values["flash_message"].(string)
}
