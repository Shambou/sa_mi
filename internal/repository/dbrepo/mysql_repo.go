package dbrepo

import (
	"context"
	"database/sql"
	"os"
	"time"

	"bitbucket.org/Shambou/coding-challenge/internal/config"
	"bitbucket.org/Shambou/coding-challenge/internal/repository"
	_ "github.com/go-sql-driver/mysql"
)

type MysqlDbRepo struct {
	App *config.AppConfig
	DB  *sql.DB
}

const maxOpenDbConn = 10
const maxIdleDbConn = 5
const maxDbLifetime = 5 * time.Minute

// NewMySqlRepo - returns database repository
func NewMySqlRepo(a *config.AppConfig) repository.DatabaseRepo {
	dsn := os.Getenv("DB_URL")

	conn, err := sql.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}

	conn.SetMaxOpenConns(maxOpenDbConn)
	conn.SetMaxIdleConns(maxIdleDbConn)
	conn.SetConnMaxLifetime(maxDbLifetime)

	return &MysqlDbRepo{
		App: a,
		DB:  conn,
	}
}

// Ping - ping database so we are sure it works
func (m *MysqlDbRepo) Ping(ctx context.Context) error {
	return m.DB.PingContext(ctx)
}
