package http

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/Shambou/coding-challenge/internal/config"
	"bitbucket.org/Shambou/coding-challenge/internal/forms"
	"bitbucket.org/Shambou/coding-challenge/internal/repository"
	"bitbucket.org/Shambou/coding-challenge/internal/repository/dbrepo"
	"github.com/gorilla/mux"
)

type Handler struct {
	Router    *mux.Router
	Server    *http.Server
	AppConfig *config.AppConfig
	DB        repository.DatabaseRepo
	Form      forms.Form
}

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// New - creates a new HTTP handler
func New(appConfig *config.AppConfig) *Handler {
	log.Println("Creating new http service")

	h := &Handler{
		AppConfig: appConfig,
		DB:        dbrepo.NewMySqlRepo(appConfig),
	}

	h.Router = mux.NewRouter()
	h.MapRoutes()

	h.Server = &http.Server{
		Addr:    ":" + os.Getenv("PORT"),
		Handler: h.Router,
		// Good practice to set timeouts to avoid slow loris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
	}

	return h
}

// Serve - gracefully serves our newly set up handler function
func (h *Handler) Serve() error {
	log.Printf("Serving app on :%s port", os.Getenv("PORT"))
	go func() {
		if err := h.Server.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	// Create a deadline to wait for
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	err := h.Server.Shutdown(ctx)
	if err != nil {
		return err
	}

	log.Println("Shutting down gracefully")
	return nil
}

// Home - Home route
func (h *Handler) Home(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/login", http.StatusMovedPermanently)
}

// ReadyCheck - Check if are connected to the database
func (h *Handler) ReadyCheck(w http.ResponseWriter, r *http.Request) {
	if err := h.DB.Ping(r.Context()); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode("I am Ready!"); err != nil {
		panic(err)
	}
}

// jsonResponse - renders json response
func jsonResponse(w http.ResponseWriter, status int, message string, data interface{}) {
	w.WriteHeader(status)
	if err := json.NewEncoder(w).Encode(Response{
		Status:  status,
		Message: message,
		Data:    data,
	}); err != nil {
		panic(err)
	}
}

// RandToken generates a random @l length token.
func RandToken(l int) (string, error) {
	b := make([]byte, l)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(b), nil
}
