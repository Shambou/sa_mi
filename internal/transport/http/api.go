package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"bitbucket.org/Shambou/coding-challenge/internal/forms"
	"bitbucket.org/Shambou/coding-challenge/internal/helpers"
	"github.com/gorilla/mux"
)

type AuthRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ProfileRequest struct {
	FullName string `json:"full_name"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
}

// ApiLogin - do a user login and store random token to the session as access token
func (h *Handler) ApiLogin(w http.ResponseWriter, r *http.Request) {
	var loginReq AuthRequest
	if err := json.NewDecoder(r.Body).Decode(&loginReq); err != nil {
		return
	}

	fmt.Println(loginReq)
	id, _, err := h.DB.Authenticate(loginReq.Email, loginReq.Password)
	if err != nil {
		jsonResponse(w, http.StatusBadRequest, "Invalid username or password", nil)
		return
	}

	accessToken, _ := RandToken(32)

	helpers.StoreSessionUser(r, w, id, accessToken)

	dataMap := make(map[string]interface{})
	dataMap["access_token"] = accessToken
	dataMap["user_id"] = id

	jsonResponse(w, http.StatusOK, "OK", dataMap)
}

// ApiGetProfile - retrieve a comment by ID
func (h *Handler) ApiGetProfile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["userId"]
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	userId, err := strconv.Atoi(id)
	if err != nil {
		jsonResponse(w, http.StatusBadRequest, "Invalid Id", nil)
		return
	}

	user, err := h.DB.GetUserById(userId)
	if err != nil {
		jsonResponse(w, http.StatusNotFound, "User not found", err)
		return
	}

	dataMap := make(map[string]interface{})
	dataMap["user"] = user

	jsonResponse(w, http.StatusOK, "OK", dataMap)
}

// ApiSignup - handles signup of the user
func (h *Handler) ApiSignup(w http.ResponseWriter, r *http.Request) {
	var signupReq AuthRequest
	if err := json.NewDecoder(r.Body).Decode(&signupReq); err != nil {
		return
	}

	formData := make(url.Values)

	formData.Add("email", signupReq.Email)
	formData.Add("password", signupReq.Password)

	form := forms.New(formData)
	form.Required("password")
	form.MinLength("password", 6)
	form.Required("email")
	form.IsEmail("email")
	if !form.Valid() {
		data := make(map[string]interface{})
		data["errors"] = form.Errors
		jsonResponse(w, http.StatusUnprocessableEntity, "Invalid data", data)
		return
	}

	_, err := h.DB.InsertUser(signupReq.Email, signupReq.Password)
	if err != nil {
		jsonResponse(w, http.StatusUnprocessableEntity, err.Error(), nil)
		return
	}

	jsonResponse(w, http.StatusOK, "Successfully registered", nil)
}

// ApiProfileEdit - handles profile update
func (h *Handler) ApiProfileEdit(w http.ResponseWriter, r *http.Request) {
	var profileReq ProfileRequest
	if err := json.NewDecoder(r.Body).Decode(&profileReq); err != nil {
		return
	}
	vars := mux.Vars(r)
	currentId, _ := strconv.Atoi(vars["userId"])
	user := h.getCurrentUser(w, r)

	fmt.Println("current id: ", currentId)
	fmt.Println("local user: ", user.ID)

	if user.ID != currentId {
		jsonResponse(w, http.StatusUnauthorized, "You can not update other user profile", nil)
		return
	}

	formData := make(url.Values)

	formData.Add("full_name", profileReq.FullName)
	formData.Add("phone", profileReq.Phone)
	formData.Add("email", profileReq.Email)

	form := forms.New(formData)
	form.Required("email")
	form.IsEmail("email")
	form.IsValidPhone("phone")
	if form.Get("full_name") != "" {
		form.MinLength("full_name", 4)
	}

	fmt.Println("form is valid: ", form.Valid())
	data := make(map[string]interface{})
	if !form.Valid() {
		data["errors"] = form.Errors
		jsonResponse(w, http.StatusUnprocessableEntity, "Invalid data", data)
		return
	}

	if user.ExternalId == "" {
		user.Email = profileReq.Email
	}
	user.FullName = profileReq.FullName
	user.Phone = profileReq.Phone

	user, err := h.DB.UpdateUser(user)
	if err != nil {
		jsonResponse(w, http.StatusUnprocessableEntity, err.Error(), nil)
		return
	}

	data["user"] = user

	jsonResponse(w, http.StatusOK, "Successfully Updated", data)
}
