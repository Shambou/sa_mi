package forms

import (
	"fmt"
	"net/mail"
	"net/url"
	"regexp"
	"strings"
)

// Form - creates a custom form struct, embeds an url.Values object
type Form struct {
	url.Values
	Errors errors
}

// Valid - returns true if there are no errors, otherwise false
func (f *Form) Valid() bool {
	return len(f.Errors) == 0
}

// New - initializes a form struct
func New(data url.Values) *Form {
	return &Form{
		data,
		errors(map[string][]string{}),
	}
}

// Required - checks for required fields
func (f *Form) Required(fields ...string) {
	for _, field := range fields {
		value := f.Get(field)
		if strings.TrimSpace(value) == "" {
			f.Errors.Add(field, "This field cannot be blank")
		}
	}
}

// MinLength - checks for string minimum length
func (f *Form) MinLength(field string, length int) bool {
	x := f.Get(field)
	if len(x) < length {
		f.Errors.Add(field, fmt.Sprintf("This field must be at least %d characters long", length))
		return false
	}
	return true
}

// IsEmail - checks for valid email address
func (f *Form) IsEmail(field string) {
	_, err := mail.ParseAddress(f.Get(field))

	if err != nil {
		f.Errors.Add(field, "Invalid email address")
	}
}

// IsValidPhone - checks for valid phone number
// code taken from https://www.golangprograms.com/regular-expression-to-validate-phone-number.html
func (f *Form) IsValidPhone(field string) {
	re := regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)

	if !re.MatchString(f.Get(field)) {
		f.Errors.Add(field, "Invalid phone number")
	}
}
