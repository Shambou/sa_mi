package dbrepo

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"bitbucket.org/Shambou/coding-challenge/internal/models"
	"github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

// Authenticate - checks if user/password combination exists in database
func (m *MysqlDbRepo) Authenticate(email string, testPassword string) (int, string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var id int
	var hashedPassword string

	row := m.DB.QueryRowContext(ctx, "select id, password from users where email = ?", email)
	err := row.Scan(&id, &hashedPassword)
	if err != nil {
		return id, "", err
	}

	err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(testPassword))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return 0, "", errors.New("incorrect password")
	} else if err != nil {
		return 0, "", err
	}

	return id, hashedPassword, nil
}

// GetUserById - get user by id from db
func (m *MysqlDbRepo) GetUserById(id int) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var user = models.User{}

	row := m.DB.QueryRowContext(
		ctx,
		"select id, COALESCE(full_name, '') full_name, email, COALESCE(phone, '') phone, COALESCE(external_id, '') external_id, created_at, updated_at from users where id = ?",
		id,
	)
	err := row.Scan(&user.ID, &user.FullName, &user.Email, &user.Phone, &user.ExternalId, &user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		return models.User{}, err
	}

	return user, nil
}

// GetUserByEmail - get user by email from db
func (m *MysqlDbRepo) GetUserByEmail(email string) (models.User, error) {
	fmt.Println("user email passed: ", email)
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var user = models.User{}

	row := m.DB.QueryRowContext(
		ctx,
		"select id, COALESCE(full_name, '') full_name, email, COALESCE(phone, '') phone, COALESCE(external_id, '') external_id, created_at, updated_at from users where email = ?",
		email,
	)
	err := row.Scan(&user.ID, &user.FullName, &user.Email, &user.Phone, &user.ExternalId, &user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		log.Println(err)
		return models.User{}, err
	}

	return user, nil
}

// InsertUser - insert new user to db
func (m *MysqlDbRepo) InsertUser(email string, password string) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var id int64
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), 14)

	stmt := `INSERT INTO users (email, password, created_at, updated_at) VALUES (?, ?, ?, ?)`

	res, err := m.DB.ExecContext(ctx, stmt,
		email,
		hashedPassword,
		time.Now(),
		time.Now(),
	)
	if err != nil {
		if err.(*mysql.MySQLError).Number == 1062 {
			return 0, errors.New("email already exists in database")
		}

		return 0, errors.New("could not insert user into database")
	}

	id, err = res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

// InsertOauthUser - insert new google user to db
func (m *MysqlDbRepo) InsertOauthUser(email string, externalId string) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var id int64
	stmt := `INSERT INTO users (email, external_id, created_at, updated_at) VALUES (?, ?, ?, ?)`

	res, err := m.DB.ExecContext(ctx, stmt,
		email,
		externalId,
		time.Now(),
		time.Now(),
	)
	if err != nil {
		if err.(*mysql.MySQLError).Number == 1062 {
			return 0, errors.New("email already exists in database")
		}

		return 0, errors.New("could not insert user into database")
	}

	id, err = res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

// UpdateUser - update user
func (m *MysqlDbRepo) UpdateUser(user models.User) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	stmt := `UPDATE users set email = ?, full_name = ?, phone = ?, external_id = ?, updated_at = ? WHERE id = ?`

	_, err := m.DB.ExecContext(ctx, stmt,
		user.Email,
		user.FullName,
		user.Phone,
		user.ExternalId,
		time.Now(),
		user.ID,
	)
	if err != nil {
		log.Println(err)
		return user, errors.New("could not update user database")
	}

	return user, nil
}
