package http

import "net/http"

// MapRoutes - maps the routes to the handlers
func (h *Handler) MapRoutes() {
	h.Router.HandleFunc("/", h.Home).Methods(http.MethodGet)
	h.Router.HandleFunc("/ready", h.ReadyCheck).Methods(http.MethodGet)

	guestRouter := h.Router.Methods(http.MethodPost, http.MethodGet).Subrouter()
	guestRouter.HandleFunc("/signup", h.ShowSignup).Methods(http.MethodGet)
	guestRouter.HandleFunc("/login", h.ShowLogin).Methods(http.MethodGet)
	guestRouter.HandleFunc("/login-gl", h.HandleGoogleLogin).Methods(http.MethodGet)
	guestRouter.HandleFunc("/callback-gl", h.CallBackFromGoogle).Methods(http.MethodGet)
	guestRouter.Use(Guest)

	authedRouter := h.Router.Methods(http.MethodPost, http.MethodGet).Subrouter()
	authedRouter.HandleFunc("/profile", h.ShowProfile).Methods(http.MethodGet)
	authedRouter.HandleFunc("/profile-edit", h.ShowProfileEdit).Methods(http.MethodGet)
	authedRouter.HandleFunc("/logout", h.Logout).Methods(http.MethodGet)
	authedRouter.HandleFunc("/oauth", h.ShowOauth).Methods(http.MethodGet)
	authedRouter.Use(Auth)

	apiRouter := h.Router.Methods(http.MethodPost, http.MethodGet, http.MethodPatch).PathPrefix("/api").Subrouter()
	apiRouter.HandleFunc("/login", h.ApiLogin).Methods(http.MethodPost)
	apiRouter.HandleFunc("/signup", h.ApiSignup).Methods(http.MethodPost)
	apiRouter.HandleFunc("/profile/{userId}", h.BasicAuthMiddleware(h.ApiGetProfile)).Methods(http.MethodGet)
	apiRouter.HandleFunc("/profile/{userId}/edit", h.BasicAuthMiddleware(h.ApiProfileEdit)).Methods(http.MethodPatch)
	apiRouter.Use(JSONMiddleware)

	h.Router.PathPrefix("/").Handler(http.FileServer(http.Dir("./web/static")))
}
