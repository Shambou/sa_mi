module bitbucket.org/Shambou/coding-challenge

// +heroku goVersion go1.18
go 1.18

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/joho/godotenv v1.4.0
	golang.org/x/crypto v0.0.0-20220408190544-5352b0902921
	golang.org/x/oauth2 v0.0.0-20220309155454-6242fa91716a
)

require (
	cloud.google.com/go v0.65.0 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/gorilla/securecookie v1.1.1 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
