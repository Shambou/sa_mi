package http

import (
	"log"
	"net/http"

	"bitbucket.org/Shambou/coding-challenge/internal/forms"
	"bitbucket.org/Shambou/coding-challenge/internal/helpers"
	"bitbucket.org/Shambou/coding-challenge/internal/models"
	"bitbucket.org/Shambou/coding-challenge/internal/render"
)

// ShowProfile - render the profile page
func (h *Handler) ShowProfile(w http.ResponseWriter, r *http.Request) {
	user := h.getCurrentUser(w, r)
	data := make(map[string]interface{})
	data["user"] = user

	err := render.Template(w, r, "profile.page.tmpl", &models.TemplateData{
		Form: forms.New(nil),
		Data: data,
	})

	if err != nil {
		log.Println(err)
		return
	}
}

// ShowProfileEdit - render the profile edit page
func (h *Handler) ShowProfileEdit(w http.ResponseWriter, r *http.Request) {
	user := h.getCurrentUser(w, r)
	data := make(map[string]interface{})
	data["user"] = user

	err := render.Template(w, r, "profile_edit.page.tmpl", &models.TemplateData{
		Form: forms.New(nil),
		Data: data,
	})
	if err != nil {
		log.Println(err)
		return
	}
}

// getCurrentUser - get user id from session and try to load full user object from database
func (h *Handler) getCurrentUser(w http.ResponseWriter, r *http.Request) models.User {
	userId := helpers.GetSessionUser(r)
	user, err := h.DB.GetUserById(userId)
	if err != nil {
		log.Println("Error when getting current user: ", user)
		log.Println("Actual error: ", err)
		log.Println("User Id passed: ", userId)
		helpers.RemoveSessionUser(r, w)
	}
	return user
}
