package models

import "bitbucket.org/Shambou/coding-challenge/internal/forms"

// TemplateData - holds data sent from handlers to templates
type TemplateData struct {
	Data            map[string]interface{}
	IsAuthenticated int
	Form            *forms.Form
	Flash           string
}
