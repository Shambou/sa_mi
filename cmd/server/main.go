package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/Shambou/coding-challenge/internal/config"
	"bitbucket.org/Shambou/coding-challenge/internal/helpers"
	"bitbucket.org/Shambou/coding-challenge/internal/render"
	transportHttp "bitbucket.org/Shambou/coding-challenge/internal/transport/http"
	"github.com/gorilla/sessions"
	"github.com/joho/godotenv"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var app config.AppConfig

// Run - sets up our application
func Run() error {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}

	app.InProduction = os.Getenv("ENVIRONMENT") == "production"
	app.Session = sessions.NewFilesystemStore("", []byte(os.Getenv("SESSION_KEY")))

	app.GoogleConf = &oauth2.Config{
		ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
		ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
		RedirectURL:  fmt.Sprintf("%s/callback-gl", os.Getenv("GOOGLE_CALLBACK_DOMAIN")),
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	app.GoogleOauthStateString = os.Getenv("OAUTH_STATE_STRING")

	render.NewRenderer(&app)
	helpers.NewHelpers(&app)

	httpHandler := transportHttp.New(&app)

	if err := httpHandler.Serve(); err != nil {
		log.Println("Failed to run http server: ", err)
		return err
	}

	return nil
}

func main() {
	log.Println("Starting the coding challenge")

	if err := Run(); err != nil {
		log.Println(err)
		log.Fatal("Error starting application")
	}
}
