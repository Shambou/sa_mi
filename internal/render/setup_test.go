package render

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"testing"

	"bitbucket.org/Shambou/coding-challenge/internal/config"
	"bitbucket.org/Shambou/coding-challenge/internal/helpers"
	"github.com/gorilla/sessions"
	"github.com/joho/godotenv"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var testApp config.AppConfig

func TestMain(m *testing.M) {
	testApp.InProduction = false
	err := godotenv.Load("../../.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	testApp.Session = sessions.NewFilesystemStore("", []byte(os.Getenv("SESSION_KEY")))
	testApp.GoogleConf = &oauth2.Config{
		ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
		ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
		RedirectURL:  fmt.Sprintf("http://localhost:%s/callback-gl", os.Getenv("PORT")),
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	testApp.GoogleOauthStateString = os.Getenv("OAUTH_STATE_STRING")

	app = &testApp
	helpers.NewHelpers(app)
	os.Exit(m.Run())
}

type myWriter struct{}

func (tw *myWriter) Header() http.Header {
	var h http.Header
	return h
}

func (tw *myWriter) WriteHeader(i int) {

}

func (tw *myWriter) Write(b []byte) (int, error) {
	length := len(b)
	return length, nil
}
