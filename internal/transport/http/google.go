package http

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"bitbucket.org/Shambou/coding-challenge/internal/helpers"
)

type GoogleResponse struct {
	Id    string
	Email string
}

// HandleGoogleLogin - handles google login
func (h *Handler) HandleGoogleLogin(w http.ResponseWriter, r *http.Request) {

	URL, err := url.Parse(h.AppConfig.GoogleConf.Endpoint.AuthURL)

	if err != nil {
		log.Println("Parse: " + err.Error())
	}

	parameters := url.Values{}
	parameters.Add("client_id", h.AppConfig.GoogleConf.ClientID)
	parameters.Add("scope", strings.Join(h.AppConfig.GoogleConf.Scopes, " "))
	parameters.Add("redirect_uri", h.AppConfig.GoogleConf.RedirectURL)
	parameters.Add("response_type", "code")
	parameters.Add("state", h.AppConfig.GoogleOauthStateString)
	URL.RawQuery = parameters.Encode()
	log.Println(h.AppConfig.GoogleConf)
	url := URL.String()

	log.Println(url)

	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

// CallBackFromGoogle - Handles google oauth callback
func (h *Handler) CallBackFromGoogle(w http.ResponseWriter, r *http.Request) {
	state := r.FormValue("state")
	if state != h.AppConfig.GoogleOauthStateString {
		log.Println("invalid oauth state, expected " + h.AppConfig.GoogleOauthStateString + ", got " + state + "\n")
		helpers.SetFlashMessage(r, w, "Something went wrong, please contact admin!")
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")
	log.Println(code)

	if code == "" {
		w.Write([]byte("Code Not Found to provide AccessToken..\n"))
		reason := r.FormValue("error_reason")
		helpers.SetFlashMessage(r, w, reason)
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
	} else {
		token, err := h.AppConfig.GoogleConf.Exchange(context.Background(), code)
		if err != nil {
			log.Println("oauthConf.Exchange() failed with " + err.Error() + "\n")
			return
		}

		resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + url.QueryEscape(token.AccessToken))
		if err != nil {
			log.Println("Get: " + err.Error() + "\n")
			helpers.SetFlashMessage(r, w, "Something went wrong, please contact admin!")
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}
		defer resp.Body.Close()

		response, err := ioutil.ReadAll(resp.Body)
		log.Println(string(response))
		if err != nil {
			log.Println("ReadAll: " + err.Error() + "\n")
			helpers.SetFlashMessage(r, w, "Something went wrong, please contact admin!")
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}

		res := GoogleResponse{}
		_ = json.Unmarshal(response, &res)
		fmt.Println(fmt.Sprintf("Res object is: %s", res))

		user, err := h.DB.GetUserByEmail(res.Email)
		accessToken, _ := RandToken(32)
		if err != nil {
			fmt.Println("failed to fetch user by email", user)
			// we don't have user try creating one
			userId, err := h.DB.InsertOauthUser(res.Email, res.Id)
			if err != nil {
				log.Println("failed to insert new oauth user", err)
				helpers.SetFlashMessage(r, w, "Something went wrong, please contact admin!")
				http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
				return
			}

			helpers.StoreSessionUser(r, w, userId, accessToken)
			http.Redirect(w, r, "/oauth", http.StatusTemporaryRedirect)
			return
		}
		if user.ExternalId == "" {
			user.ExternalId = res.Id
			user, err = h.DB.UpdateUser(user)
			if err != nil {
				log.Println("failed to update oauth user", err)
				helpers.SetFlashMessage(r, w, "Something went wrong, please contact admin!")
				http.Redirect(w, r, "/oauth", http.StatusTemporaryRedirect)
				return
			}
		}

		helpers.StoreSessionUser(r, w, user.ID, accessToken)
		http.Redirect(w, r, "/oauth", http.StatusTemporaryRedirect)
	}
}
