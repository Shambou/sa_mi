package http

import (
	"log"
	"net/http"

	"bitbucket.org/Shambou/coding-challenge/internal/forms"
	"bitbucket.org/Shambou/coding-challenge/internal/helpers"
	"bitbucket.org/Shambou/coding-challenge/internal/models"
	"bitbucket.org/Shambou/coding-challenge/internal/render"
)

// ShowLogin - show login form
func (h *Handler) ShowLogin(w http.ResponseWriter, r *http.Request) {
	err := render.Template(w, r, "login.page.tmpl", &models.TemplateData{
		Form: forms.New(nil),
	})

	if err != nil {
		log.Println(err)
		return
	}
}

// ShowSignup - show signup form
func (h *Handler) ShowSignup(w http.ResponseWriter, r *http.Request) {
	err := render.Template(w, r, "signup.page.tmpl", &models.TemplateData{
		Form: forms.New(nil),
	})
	if err != nil {
		log.Panic(err)
		return
	}
}

// ShowOauth - load the template so we can store the access token in local storage
func (h *Handler) ShowOauth(w http.ResponseWriter, r *http.Request) {
	data := make(map[string]interface{})

	data["access_token"] = helpers.GetSessionAccessToken(r)
	data["user_id"] = helpers.GetSessionUser(r)

	err := render.Template(w, r, "oauth.page.tmpl", &models.TemplateData{
		Data: data,
	})
	if err != nil {
		log.Panic(err)
		return
	}
}

// Logout - removes user from session
func (h *Handler) Logout(w http.ResponseWriter, r *http.Request) {
	helpers.RemoveSessionUser(r, w)
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}
