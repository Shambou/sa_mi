package config

import (
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
)

type AppConfig struct {
	InProduction           bool
	Session                *sessions.FilesystemStore
	GoogleConf             *oauth2.Config
	GoogleOauthStateString string
}
