package repository

import (
	"context"

	"bitbucket.org/Shambou/coding-challenge/internal/models"
)

type DatabaseRepo interface {
	Ping(ctx context.Context) error
	Authenticate(email string, testPassword string) (int, string, error)
	InsertUser(email string, password string) (int, error)
	InsertOauthUser(email string, externalId string) (int, error)
	GetUserById(id int) (models.User, error)
	GetUserByEmail(email string) (models.User, error)
	UpdateUser(user models.User) (models.User, error)
}
