package render

import (
	"net/http"
	"testing"

	"bitbucket.org/Shambou/coding-challenge/internal/forms"
	"bitbucket.org/Shambou/coding-challenge/internal/models"
)

func TestAddDefaultData(t *testing.T) {
	var td models.TemplateData

	r, _ := http.NewRequest("GET", "/some-url", nil)
	_ = AddDefaultData(&td, r)
}

func TestRenderTemplate(t *testing.T) {
	pathToTemplates = "./../../web/views"
	_, err := generateTemplates()
	if err != nil {
		t.Error(err)
	}

	r, _ := http.NewRequest("GET", "/some-url", nil)
	var ww myWriter

	err = Template(&ww, r, "login.page.tmpl", &models.TemplateData{
		Form: forms.New(nil),
	})
	if err != nil {
		t.Error("error writing template to browser")
	}

	err = Template(&ww, r, "non-existent.page.tmpl", &models.TemplateData{})
	if err == nil {
		t.Error("rendered template that does not exist")
	}
}

func TestNewTemplates(t *testing.T) {
	NewRenderer(app)
}

func TestGenerateTemplates(t *testing.T) {
	pathToTemplates = "./../../web/views"
	_, err := generateTemplates()
	if err != nil {
		t.Error(err)
	}
}
